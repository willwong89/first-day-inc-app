const functions = require('firebase-functions');
const admin = require('firebase-admin');
const axios = require('axios');
const config = require('./config.json');

admin.initializeApp();

exports.swapSku = functions.https.onRequest(async (request, response) => {
  response.sendStatus(200);

  try {
    // get this mapping from Firebase db
    const variantIdSwap = (await admin
      .database()
      .ref('variantIdSwap')
      .once('value')).val();

    // get the request data sent from the webhook subscribed to with recharge
    const requestData = request.body;
    console.log(requestData);

    const originalVariantId = requestData.subscription.shopify_variant_id;

    // if we have a variant swap pair, then swap the variant id in this recharge subscription
    if (variantIdSwap[originalVariantId]) {
      const res = await axios.put(
        `https://api.rechargeapps.com/subscriptions/${requestData.subscription.id}`,
        {
          shopify_variant_id: variantIdSwap[originalVariantId].to
        },
        {
          headers: {
            'X-Recharge-Access-Token': config.rechargeApiKey
          }
        }
      );
      console.log(res);
      console.log(
        `Swapped variant ID ${originalVariantId} with ${variantIdSwap[originalVariantId].to} for subscription ID ${requestData.subscription.id}`
      );
    }
  } catch (error) {
    console.error(error);
  }
});
