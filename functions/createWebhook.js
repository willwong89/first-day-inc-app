/*
curl -i -H 'X-Recharge-Access-Token: your_api_token' \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-X POST https://api.rechargeapps.com/webhooks \
--data '{"address":"https://request.in/foo", "topic":"subscription/created"}'

*/

const axios = require('axios');
const config = require('./config.json');

const createWebhook = async url => {
  console.log(config.rechargeApiKey);
  console.log(url);
  const res = await axios.post(
    'https://api.rechargeapps.com/webhooks',
    {
      address: url,
      topic: 'subscription/created'
    },
    {
      headers: {
        'X-Recharge-Access-Token': config.rechargeApiKey
      }
    }
  );
  console.log(res);
};

createWebhook(
  'https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku'
);
