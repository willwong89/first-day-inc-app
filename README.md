# first-day-inc

Shopify store: https://first-day-in-dev.myshopify.com/admin/settings/notifications

Recharge API: https://developer.rechargepayments.com/#webhook-response-examples

process:

- listen for 'subscription/created' with webhook
- defined mapping variant ID from to in Firebase realtime db
- when webhook is called, if a mapping exists, then swap the product in the subscription,
  using https://developer.rechargepayments.com/#swap-product-new

# Notes

### Created Webhook

```
{
  status: 200,
  statusText: 'OK',
  headers: {
    server: 'nginx',
    date: 'Wed, 13 Nov 2019 16:57:59 GMT',
    'content-type': 'application/json',
    'content-length': '138',
    connection: 'close',
    'x-recharge-limit': '1/40',
    'set-cookie': [
      'session=eyJfcGVybWFuZW50Ijp0cnVlfQ.EK3Hlw.wk3vBAcMTys2TKTebSnTEDkz8qA; Domain=.rechargeapps.com; Expires=Sat, 14-Dec-2019 16:57:59 GMT; HttpOnly; Path=/'
    ],
    'x-request-id': 'e3025d54eb839644e0218dbade515df9'
  },
  config: {
    url: 'https://api.rechargeapps.com/webhooks',
    method: 'post',
    data: '{"address":"https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku","topic":"subscription/created"}',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json;charset=utf-8',
      'X-Recharge-Access-Token': 'cc2a3125dea35e9dce3fa58f636e4c4a4493d8164e717b37e1cc8934',
      'User-Agent': 'axios/0.19.0',
      'Content-Length': 114
    },
    transformRequest: [ [Function: transformRequest] ],
    transformResponse: [ [Function: transformResponse] ],
    timeout: 0,
    adapter: [Function: httpAdapter],
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN',
    maxContentLength: -1,
    validateStatus: [Function: validateStatus]
  },
  request: ClientRequest {
    _events: [Object: null prototype] {
      socket: [Function],
      abort: [Function],
      aborted: [Function],
      error: [Function],
      timeout: [Function],
      prefinish: [Function: requestOnPrefinish]
    },
    _eventsCount: 6,
    _maxListeners: undefined,
    outputData: [],
    outputSize: 0,
    writable: true,
    _last: true,
    chunkedEncoding: false,
    shouldKeepAlive: false,
    useChunkedEncodingByDefault: true,
    sendDate: false,
    _removedConnection: false,
    _removedContLen: false,
    _removedTE: false,
    _contentLength: null,
    _hasBody: true,
    _trailer: '',
    finished: true,
    _headerSent: true,
    socket: TLSSocket {
      _tlsOptions: [Object],
      _secureEstablished: true,
      _securePending: false,
      _newSessionPending: false,
      _controlReleased: true,
      _SNICallback: null,
      servername: 'api.rechargeapps.com',
      alpnProtocol: false,
      authorized: true,
      authorizationError: null,
      encrypted: true,
      _events: [Object: null prototype],
      _eventsCount: 9,
      connecting: false,
      _hadError: false,
      _parent: null,
      _host: 'api.rechargeapps.com',
      _readableState: [ReadableState],
      readable: true,
      _maxListeners: undefined,
      _writableState: [WritableState],
      writable: false,
      allowHalfOpen: false,
      _sockname: null,
      _pendingData: null,
      _pendingEncoding: '',
      server: undefined,
      _server: null,
      ssl: [TLSWrap],
      _requestCert: true,
      _rejectUnauthorized: true,
      parser: null,
      _httpMessage: [Circular],
      [Symbol(res)]: [TLSWrap],
      [Symbol(asyncId)]: 8,
      [Symbol(kHandle)]: [TLSWrap],
      [Symbol(lastWriteQueueSize)]: 0,
      [Symbol(timeout)]: null,
      [Symbol(kBuffer)]: null,
      [Symbol(kBufferCb)]: null,
      [Symbol(kBufferGen)]: null,
      [Symbol(kBytesRead)]: 0,
      [Symbol(kBytesWritten)]: 0,
      [Symbol(connect-options)]: [Object]
    },
    connection: TLSSocket {
      _tlsOptions: [Object],
      _secureEstablished: true,
      _securePending: false,
      _newSessionPending: false,
      _controlReleased: true,
      _SNICallback: null,
      servername: 'api.rechargeapps.com',
      alpnProtocol: false,
      authorized: true,
      authorizationError: null,
      encrypted: true,
      _events: [Object: null prototype],
      _eventsCount: 9,
      connecting: false,
      _hadError: false,
      _parent: null,
      _host: 'api.rechargeapps.com',
      _readableState: [ReadableState],
      readable: true,
      _maxListeners: undefined,
      _writableState: [WritableState],
      writable: false,
      allowHalfOpen: false,
      _sockname: null,
      _pendingData: null,
      _pendingEncoding: '',
      server: undefined,
      _server: null,
      ssl: [TLSWrap],
      _requestCert: true,
      _rejectUnauthorized: true,
      parser: null,
      _httpMessage: [Circular],
      [Symbol(res)]: [TLSWrap],
      [Symbol(asyncId)]: 8,
      [Symbol(kHandle)]: [TLSWrap],
      [Symbol(lastWriteQueueSize)]: 0,
      [Symbol(timeout)]: null,
      [Symbol(kBuffer)]: null,
      [Symbol(kBufferCb)]: null,
      [Symbol(kBufferGen)]: null,
      [Symbol(kBytesRead)]: 0,
      [Symbol(kBytesWritten)]: 0,
      [Symbol(connect-options)]: [Object]
    },
    _header: 'POST /webhooks HTTP/1.1\r\n' +
      'Accept: application/json, text/plain, */*\r\n' +
      'Content-Type: application/json;charset=utf-8\r\n' +
      'X-Recharge-Access-Token: cc2a3125dea35e9dce3fa58f636e4c4a4493d8164e717b37e1cc8934\r\n' +
      'User-Agent: axios/0.19.0\r\n' +
      'Content-Length: 114\r\n' +
      'Host: api.rechargeapps.com\r\n' +
      'Connection: close\r\n' +
      '\r\n',
    _onPendingData: [Function: noopPendingOutput],
    agent: Agent {
      _events: [Object: null prototype],
      _eventsCount: 1,
      _maxListeners: undefined,
      defaultPort: 443,
      protocol: 'https:',
      options: [Object],
      requests: {},
      sockets: [Object],
      freeSockets: {},
      keepAliveMsecs: 1000,
      keepAlive: false,
      maxSockets: Infinity,
      maxFreeSockets: 256,
      maxCachedSessions: 100,
      _sessionCache: [Object]
    },
    socketPath: undefined,
    method: 'POST',
    path: '/webhooks',
    _ended: true,
    res: IncomingMessage {
      _readableState: [ReadableState],
      readable: false,
      _events: [Object: null prototype],
      _eventsCount: 3,
      _maxListeners: undefined,
      socket: [TLSSocket],
      connection: [TLSSocket],
      httpVersionMajor: 1,
      httpVersionMinor: 1,
      httpVersion: '1.1',
      complete: true,
      headers: [Object],
      rawHeaders: [Array],
      trailers: {},
      rawTrailers: [],
      aborted: false,
      upgrade: false,
      url: '',
      method: null,
      statusCode: 200,
      statusMessage: 'OK',
      client: [TLSSocket],
      _consuming: false,
      _dumped: false,
      req: [Circular],
      responseUrl: 'https://api.rechargeapps.com/webhooks',
      redirects: []
    },
    aborted: false,
    timeoutCb: null,
    upgradeOrConnect: false,
    parser: null,
    maxHeadersCount: null,
    _redirectable: Writable {
      _writableState: [WritableState],
      writable: true,
      _events: [Object: null prototype],
      _eventsCount: 2,
      _maxListeners: undefined,
      _options: [Object],
      _redirectCount: 0,
      _redirects: [],
      _requestBodyLength: 114,
      _requestBodyBuffers: [],
      _onNativeResponse: [Function],
      _currentRequest: [Circular],
      _currentUrl: 'https://api.rechargeapps.com/webhooks'
    },
    [Symbol(kNeedDrain)]: false,
    [Symbol(isCorked)]: false,
    [Symbol(kOutHeaders)]: [Object: null prototype] {
      accept: [Array],
      'content-type': [Array],
      'x-recharge-access-token': [Array],
      'user-agent': [Array],
      'content-length': [Array],
      host: [Array]
    }
  },
  data: {
    webhook: {
      address: 'https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku',
      id: 73617,
      topic: 'subscription/created'
    }
  }
}
```

# Archived notes

---

Swap SKU if customer orders for the second time

to ascertain how many times they have ordered a product, use:

```
GET /orders?customer_id=CUST_ID&limit=250

and then group them by subscription_id found in order.line_items
```

Variant swap on checkout of product

Using the API for the recharge app we will be doing this

> 1. listen for subscription/created
> 2. once triggered, GET api/charges?subscription_id=<subscription_id_from_the_payload>
> 3. PUT api/subscriptions/<subscription_id>”

webhook URL to be called hosted on Firebase will be https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku

1.
webhook created

```
{
    webhook:
    {
        address: 'https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku',
        id: 68052,
        topic: 'subscription/created'
    }
}
```

listing all webhooks

```
{ webhooks:
   [ { address: 'https://www.seoratna.com/firstday/recharge.php',
       id: 67403,
       topic: 'checkout/completed' },
     { address: 'https://www.seoratna.com/firstday/hello.php',
       id: 67440,
       topic: 'checkout/completed' },
     { address:
        'https://us-central1-first-day-recharge-app.cloudfunctions.net/swapSku',
       id: 68052,
       topic: 'subscription/created' } ] }
```

## Notes

request body received to Firebase function looks like

````
{ subscription:
   { id: 53941747,
     customer_id: 34205344,
     address_id: 38572986,
     recharge_product_id: 1322233,
     shopify_product_id: 4328869068874,
     shopify_variant_id: 31073424703562,
     sku: null,
     product_title: 'Kids Enrichment - 1 Bottle  Auto renew',
     variant_title: '',
     quantity: 1,
     status: 'ACTIVE',
     sku_override: false,
     price: 1,
     charge_interval_frequency: '1',
     next_charge_scheduled_at: '2019-12-08T00:00:00',
     order_interval_frequency: '1',
     order_interval_unit: 'month',
     order_day_of_week: null,
     order_day_of_month: null,
     properties: [ [Object], [Object] ],
     has_queued_charges: 1,
     max_retries_reached: 0,
     expire_after_specific_number_of_charges: null,
     cancelled_at: null,
     cancellation_reason: null,
     cancellation_reason_comments: null,
     is_skippable: null,
     is_swappable: null,
     created_at: '2019-11-08T13:14:23',
     updated_at: '2019-11-08T13:14:23' } }
     ```

Subscription JSON:

````

{
"charges": [
{
"address_id": 38572986,
"billing_address": {
"address1": "1819 Polk St.",
"address2": "",
"city": "San Francisco",
"company": "",
"country": "United States",
"first_name": "Andy",
"last_name": "Wang",
"phone": "",
"province": "California",
"zip": "94109"
},
"client_details": {
"browser_ip": null,
"user_agent": null
},
"created_at": "2019-11-07T07:21:18",
"customer_hash": "342053445abfb0ee78bc8554",
"customer_id": 34205344,
"discount_codes": [],
"email": "support@andrewgolightly.com",
"first_name": "Andy",
"has_uncommited_changes": false,
"id": 191060746,
"last_name": "Wang",
"line_items": [
{
"grams": 312,
"images": {
"large": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_large.png",
"medium": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_medium.png",
"original": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile.png",
"small": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_small.png"
},
"price": "1.00",
"properties": [
{
"name": "shipping_interval_frequency",
"value": "1"
},
{
"name": "shipping_interval_unit_type",
"value": "Months"
}
],
"quantity": 1,
"shopify_product_id": "4328869068874",
"shopify_variant_id": "31073424703562",
"sku": "FDK",
"subscription_id": 53859238,
"title": "Kids Enrichment - 1 Bottle Auto renew",
"variant_title": ""
},
{
"grams": 567,
"images": {
"large": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_fca5fbc1-7b1e-4c28-9d42-4ce893e1f8e4_large.png",
"medium": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_fca5fbc1-7b1e-4c28-9d42-4ce893e1f8e4_medium.png",
"original": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_fca5fbc1-7b1e-4c28-9d42-4ce893e1f8e4.png",
"small": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_fca5fbc1-7b1e-4c28-9d42-4ce893e1f8e4_small.png"
},
"price": "0.18",
"properties": [
{
"name": "shipping_interval_frequency",
"value": "1"
},
{
"name": "shipping_interval_unit_type",
"value": "Months"
}
],
"quantity": 1,
"shopify_product_id": "4328871264330",
"shopify_variant_id": "31073460781130",
"sku": "",
"subscription_id": 53858880,
"title": "Kids Enrichment - 2 Bottles Auto renew",
"variant_title": ""
}
],
"note": null,
"note_attributes": null,
"processor_name": "braintree",
"scheduled_at": "2019-12-07T00:00:00",
"shipments_count": null,
"shipping_address": {
"address1": "1819 Polk St.",
"address2": "",
"city": "San Francisco",
"company": "",
"country": "United States",
"first_name": "Andy",
"last_name": "Wang",
"phone": "",
"province": "California",
"zip": "94109"
},
"shipping_lines": [
{
"code": "FREE",
"price": "0.00",
"title": "FREE"
}
],
"shopify_order_id": null,
"status": "QUEUED",
"sub_total": null,
"subtotal_price": "1.18",
"tags": "Subscription, Subscription Recurring Order",
"tax_lines": 0,
"total_discounts": "0.0",
"total_line_items_price": "1.18",
"total_price": "1.18",
"total_refunds": null,
"total_tax": 0,
"total_weight": 879,
"transaction_id": null,
"type": "RECURRING",
"updated_at": "2019-11-07T07:32:59"
}
]
}

```

```

{
"charges": [
{
"address_id": 38572986,
"billing_address": {
"address1": "1819 Polk St.",
"address2": "",
"city": "San Francisco",
"company": "",
"country": "United States",
"first_name": "Andy",
"last_name": "Wang",
"phone": "",
"province": "California",
"zip": "94109"
},
"client_details": {
"browser_ip": null,
"user_agent": null
},
"created_at": "2019-11-08T13:14:23",
"customer_hash": "342053445abfb0ee78bc8554",
"customer_id": 34205344,
"discount_codes": [],
"email": "support@andrewgolightly.com",
"first_name": "Andy",
"has_uncommited_changes": false,
"id": 191311145,
"last_name": "Wang",
"line_items": [
{
"grams": 312,
"images": {
"large": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_large.png",
"medium": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_medium.png",
"original": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile.png",
"small": "https://cdn.shopify.com/s/files/1/0266/9835/0666/products/Product_Hero_mobile_small.png"
},
"price": "1.00",
"properties": [
{
"name": "shipping_interval_frequency",
"value": "1"
},
{
"name": "shipping_interval_unit_type",
"value": "Months"
}
],
"quantity": 1,
"shopify_product_id": "4328869068874",
"shopify_variant_id": "31073424703562",
"sku": "FDK",
"subscription_id": 53941747,
"title": "Kids Enrichment - 1 Bottle Auto renew",
"variant_title": ""
}
],
"note": null,
"note_attributes": null,
"processor_name": "braintree",
"scheduled_at": "2019-12-08T00:00:00",
"shipments_count": null,
"shipping_address": {
"address1": "1819 Polk St.",
"address2": "",
"city": "San Francisco",
"company": "",
"country": "United States",
"first_name": "Andy",
"last_name": "Wang",
"phone": "",
"province": "California",
"zip": "94109"
},
"shipping_lines": [
{
"code": "FREE",
"price": "0.00",
"title": "FREE"
}
],
"shopify_order_id": null,
"status": "QUEUED",
"sub_total": null,
"subtotal_price": "1.0",
"tags": "Subscription, Subscription Recurring Order",
"tax_lines": 0,
"total_discounts": "0.0",
"total_line_items_price": "1.00",
"total_price": "1.00",
"total_refunds": null,
"total_tax": 0,
"total_weight": 312,
"transaction_id": null,
"type": "RECURRING",
"updated_at": "2019-11-08T13:14:25"
}
]
}

```

```
